<?php

namespace App\Http\Controllers;

use App\SnowGoose;
use Illuminate\Http\Request;

class SnowGooseController extends Controller
{
	
	public function __construct(Request $request)
	{
		
		$this->response = [];
		
		$this->response['status'] = 'success';
		
		
		if (!$path = $request->get('path')) {
			
			$this->response['status'] = 'failed';
			$this->response['message'] = 'Path Required.';
			
			return $this->response;
			
		} else {
			
			$this->path = $request->get('path');
			
		}

		$this->snowgoose = new SnowGoose($path);
		
	}
	
	public function init(Request $request)
	{
		
		if (!$this->snowgoose->isValidProject()) {
			
			$this->snowgoose->addMigrationError("error","Path does not lead to a valid project.",'','');
			
			if (!empty($this->snowgoose->getMigrationErrors())) {
			
				$this->response['status'] = "failed";
				
				$this->response['errors'] = $this->snowgoose->getMigrationErrors();
				
			}
			
		}
		
		$alt_path = false;
		
		$path = $this->snowgoose->getProjectRootPath();
		
		$this->response['data']['root_path'] = $this->snowgoose->getProjectRootPath();
		
		$this->response['data']['update_path'] = $this->snowgoose->getProjectUpdatePath();
		
		$this->response['data']['dependencies'] = $this->snowgoose->getProjectDependencies($path);

		$this->response['data']['file_tree'] = \View::make('_partials.migration_file', ['tree' => $this->snowgoose->getProjectTree($path), 'alt_path' => false])->render();
		
		if (!empty($this->snowgoose->getMigrationErrors())) {
			
			$this->response['status'] = "failed";
			
			$this->response['errors'] = $this->snowgoose->getMigrationErrors();
			
		}
		
		return json_encode($this->response);
		
	}
	
	
	
	public function installUpdate(Request $request)
	{
		
		$path = $this->snowgoose->getProjectRootPath();
		
		$update_path = $this->snowgoose->getProjectUpdatePath();
		
		if (!$request->get('version')) {
			
			$this->snowgoose->addMigrationError('error','Unable to get project version',$path,$update_path);
			
		}
		
		if ($this->snowgoose->updateExists()) {
			
			$this->snowgoose->removeContents($update_path);
			
		}
		
		$this->snowgoose->createUpdateDirectory();
		
		
		
		if (!$output = $this->snowgoose->downloadPackage($request->get('version'))) {
		
			$this->snowgoose->addMigrationError('error','Unable to download package.',$path,$update_path);
		
		}
		
		if (!empty($this->snowgoose->getMigrationErrors())) {
			
			$this->response['status'] = "failed";
			
			$this->response['errors'] = $this->snowgoose->getMigrationErrors();
			
		}
		
		$this->response['data']['file_tree'] = \View::make('_partials.migration_file', ['tree' => $this->snowgoose->getProjectTree($update_path), 'alt_path' => true])->render();
		
		return json_encode($this->response);
		
	}
	

	
	public function migrate(Request $request) {
		
		# Make sure the neccessary component are available
		if (!$request->get('migration_list')) {

			$this->snowgoose->addMigrationError("error","Unable to get migration list.",'','');
			
			$this->response['status'] = "failed";
			
			$this->response['errors'] = $this->snowgoose->getMigrationErrors();
			
			return json_encode($this->response);
			
		}
		
		if (!file_exists($this->snowgoose->getProjectRootPath())) {
			
			$this->snowgoose->addMigrationError("error","Root Directory does not exist.",'','');
			
			$this->response['status'] = "failed";
			
			$this->response['errors'] = $this->snowgoose->getMigrationErrors();
			
			return json_encode($this->response);
			
		}
		
		if (!file_exists($this->snowgoose->getProjectUpdatePath())) {
			
			$this->snowgoose->addMigrationError("error","Update Directory does not exist.",'','');
			
			$this->response['status'] = "failed";
			
			$this->response['errors'] = $this->snowgoose->getMigrationErrors();
			
			return json_encode($this->response);
			
		}

		if (file_exists($this->snowgoose->getProjectArchivePath())) {
			
			$counter = 0;
			
			while(file_exists($this->snowgoose->getProjectArchivePath() . "." . $counter)) {
				
				$counter++;
				
			}
			
			rename($this->snowgoose->getProjectArchivePath(),$this->snowgoose->getProjectArchivePath() . "." . $counter);
			
		}


		#Variable List
		$root_path = $this->snowgoose->getProjectRootPath();
		
		$update_path = $this->snowgoose->getProjectUpdatePath();
		
		$archive_path = $this->snowgoose->getProjectArchivePath();
		
		$migration_list = json_decode($request->get('migration_list'),true);
		
		$force_migration = ($request->get('force_migration')) ? true : false;
		
		array_push($this->snowgoose->exclude,'composer.json'); //Pull this from a method
		
		$root_composer_config_path = $root_path . "/composer.json"; //Pull this from a method
		
		$update_composer_config_path = $update_path . "/composer.json"; //Pull this from a method
		
		#Begin Dirty Work...
		
		#Move all the files from the root project to the update directory.
		foreach ($migration_list as $item) {
			
			if (in_array(basename($item['source']), $this->snowgoose->exclude)) continue;
			
			$item_source_path = $this->snowgoose->getProjectRootPath() . '/' . $item['source'];
			
			$item_destination_path = $this->snowgoose->getProjectUpdatePath() . '/' . $item['destination'];
			
			if (!file_exists($item_source_path)) {

				$this->snowgoose->addMigrationError("error","Source file does not exist",$item_source_path,$item_destination_path);

				continue;
			}
			
			if ($this->snowgoose->isDirectory($item_source_path)) {
				
				if (!$this->snowgoose->migrateFile($item_source_path,$item_destination_path,['directory' => true])) {
					
					$this->snowgoose->addMigrationError("error","Unable to move $item_source_path.",$root_path,$update_path);
					
				}
				
			}
			
			if ($this->snowgoose->isFile($item_source_path)) {
				
				if (!$this->snowgoose->migrateFile($item_source_path,$item_destination_path)) {
					
					$this->snowgoose->addMigrationError("error","Unable to move $item_source_path.",$root_path,$update_path);
					
				}
				
			}
			
		}
		
		# Move ENV
		if (!$this->snowgoose->migrateENV()) {
			$this->snowgoose->addMigrationError("error","Unable to migrate ENV file.",$root_path,$update_path);
		}
		
		#Migrate Git
		if ($this->snowgoose->hasGit() && !$this->snowgoose->migrateGit()) {
			$this->snowgoose->addMigrationError("error","Could not migrate Git.",$root_path,$update_path);
		}

		#Migrate Gulp
		if ($this->snowgoose->hasGulp() && !$this->snowgoose->migrateGulp()) {
			$this->snowgoose->addMigrationError("error","Could not migrate Git.",$root_path,$update_path);
		}
		
		# Migrate Node Modules
		if ($this->snowgoose->hasNodeModules() && !$this->snowgoose->migrateNodeModules()) {
			$this->snowgoose->addMigrationError("error","Could not migrate node dependencies.",$root_path,$update_path);
		}
		
		# Merge the root composer.json with the fresh copy of composer.json
		if (!$this->snowgoose->migrateComposerConfig($root_composer_config_path,$update_composer_config_path)) {
			$this->snowgoose->addMigrationError("error","Unable to migrate composer.json",$root_path,$update_path);
		}
		
		# Update the composer.lock file.
		if (!$this->snowgoose->composerUpdate($update_path)) {
			$this->snowgoose->addMigrationError("error","Unable to update composer.",$root_path,$update_path);
		}
		
		# Install new composer dependencies
		if (!$this->snowgoose->composerInstall($update_path)) {
			$this->snowgoose->addMigrationError("error","Unable to install composer dependencies.",$root_path,$update_path);
		}
		
		# Dump composers autoload
		if (!$this->snowgoose->composerDumpAutoload($update_path)) {
			$this->snowgoose->addMigrationError("error","Unable to dump autoload.",$root_path,$update_path);
		}
		
		# Archive root project
		if (!rename($root_path,$archive_path)) {
			$this->snowgoose->addMigrationError("error","Unable to archive root project.",$root_path,$update_path);
		}

		# Move update to root path
		if (!rename($update_path,$root_path)) {
			$this->snowgoose->addMigrationError("error","Unable to move update to root path.",$root_path,$update_path);
		}
		
		if (!empty($this->snowgoose->getMigrationErrors())) {
			
			$this->response['status'] = "failed";
			
			$this->response['errors'] = $this->snowgoose->getMigrationErrors();
			
		}
	
		return json_encode($this->response);
	}
}