
$(document).on('click','.directory-trigger',function(e){
	
	console.info("Toggling " + $(this).attr('data-target') + ".");
	
	var $target = $(".builder " + $(this).attr('data-target'));
	
	if ($target.css('display') == 'none') {
		
		$target.show();
		
		$(this).siblings('.glyphicon').last().removeClass('glyphicon-triangle-right').addClass('glyphicon-triangle-bottom');
		
	} else if (($target.css('display') == 'block') && e.target.localName !== 'input') {
		
		$target.hide();
		
		$(this).siblings('.glyphicon').last().removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');
		
	}	
	
});



$(document).on('click','#migration-builder .builder .select-children',function(e){
	
	console.info("Selecting children.");
	
	targetId = $(this).attr('data-target');
	
	MigrationBuilder.selectChildren($(targetId),e.target.checked);
	
});



$(document).on('click','#migration-builder .builder .file input, #migration-builder .builder .directory input',function(e) {
	
	var sourcePath = $(this).val();
	
	console.info("Adding file to migration.(" + sourcePath + ")");
	
	if (e.target.checked && sourcePath !== "*") {
		
		MigrationBuilder.addMigrationEntry(sourcePath);
		
	} else {
		
		MigrationBuilder.removeMigrationEntry(sourcePath);
		
	}
	
});


$(document).on('click','.select-all-files',function(e){
	
	StatusUpdate.openModal('Warning','Heads up. Selecting all files could result in newer files being over-written, causing the migrated project to perform unexpectedly.');
	
	var $allFiles = $('#migration-builder .builder li input');
	
	if (e.target.checked) {
		
		console.info("Selecting all files");
		
		$allFiles
		.prop('checked',true)
		.each(function(key,val){
			
			MigrationBuilder.addMigrationEntry(val.value);
			
		});
		
	} else {
		
		console.info("deselecting all files");
		
		$allFiles
		.prop('checked',false)
		.each(function(key,val){
			
			MigrationBuilder.removeMigrationEntry(val.value);
			
		});
		
	}
	
});

$(document).on('click','.launch-alt-path',function(){

	var path = $(this).attr('data-source');
	
	MigrationBuilder.openAltPathModal();
	
	MigrationBuilder.setAltSource(path);
	
});

$(document).on('click','.save-alt-path',function(){

	var altSourcePath = MigrationBuilder.getAltSource();

	var altDestinationPath = $('#alt-migration-modal input:checked').val();

	MigrationBuilder.addMigrationEntry(altSourcePath,altDestinationPath);
	
	MigrationBuilder.closeAltPathModal();

});
