$(document).on('click', '#run-migration', function() {
	
	StatusUpdate.openModal("Migrating files","Replacing files, updating dependencies, archiving old project...");
	
	StatusUpdate.loaderOn();
	
	var path = SnowGoose.getRootPath();
	
	var migrationList = MigrationBuilder.getMigrationList();
	
	HttpRunner.migrate(path,migrationList)
	.then(function(response){
		
		console.log(response);
		
		response = JSON.parse(response);
		
		if (response.status === "failed") {
			
			StatusUpdate.setMessage("Unfortunately SnowGoose had trouble migrating for following reasons:");
			
			StatusUpdate.addError(response.errors);
			
			return false;
			
		} else {
			
			StatusUpdate.showSuccess("Project has been successfully migrated!");
		
			StatusUpdate.loaderOff();
			
		}
		
	});

});
