var HttpRunner = (function() {
	
	var isValidProject = function(path) {
		
		var endpoint = "/api/init";
		
		var requestType = "POST";
		
		return $http(endpoint,requestType,path);
		
	}
	
	var installProjectUpdate= function(path,version) {

		var endpoint = "/api/install_update";
		
		var requestType = "POST";
		
		var additionalParameters = "&version=" + version;
		
		console.log("path: " + path);
		
		return $http(endpoint,requestType,path,additionalParameters);

	}
	
	var migrate = function(path,targetPath,migrationList) {
		
		if (typeof path == "undefined" || path === "") {
			
			path = MigrationBuilder.getRootProjectPath();
			
		}
		
		if (typeof targetPath == "undefined" || targetPath === "") {
			
			targetPath = MigrationBuilder.getUpdateProjectPath(); // move this outside the file?
			
		}
		
		if (typeof migrationList == "undefined" || migrationList === "") {
			
			migrationList = JSON.stringify(MigrationBuilder.getMigrationList());
			
		}
		
		var endpoint = "/api/migrate";
		
		var requestType = "POST";
		
		var additionalParameters = "&target_path=" + targetPath + "&migration_list=" + migrationList; // Factor requestType and path out of all these functions***

		return $http(endpoint,requestType,path,additionalParameters);
		
	}
	
	var $http = function(endpoint,requestType,path,additionalParameters="") {
		
		return new Promise(function(resolve,reject) {
			
			var xhr = new XMLHttpRequest();
				
				xhr.onload = function() {
					
					resolve(this.responseText);
					
				};
				
				xhr.onerror = reject;
				
				xhr.open(requestType, endpoint);
				
				xhr.setRequestHeader('X-CSRF-TOKEN',window.csrfToken);
				
				xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

				xhr.send("path=" + path + additionalParameters);
			
		});
		
	}
	
	return {
		installProjectUpdate : installProjectUpdate,
		isValidProject : isValidProject,
		migrate : migrate
	}
	
})();