StatusUpdate = (function(){
	
	properties = {
		selector : "#status-update"
	}
	
	var init = function(title,message) {
		
		console.log("Initializing update modal");
		
		if (typeof title == "undefined") title = "";
		if (typeof message == "undefined") message = "";
		
		$(properties.selector + " .modal-header .title").text(title);
		
		$(properties.selector + " .modal-body .message").text(message);
		
		$(properties.selector + " .modal-body .status").html("");
		
	}
	
	var hideButtons = function() {
		
		$(properties.selector + " button").hide();
		
	}
	
	var showButtons = function() {
		
		$(properties.selector + " button").fadeIn();
		
	}
	
	var openModal = function(title,message){
		
		init(title,message);
		
		$(properties.selector).modal('show');
		
	}
	
	var setSelector = function(selector){
		
		properties.selector = selector;
		
	}
	
	var addUpdate = function(message) {
	
		$('<li></li>').text(message).delay(500).appendTo(properties.selector + ' .status');
		
	}
	
	var addError = function(errors){
		
		$.each(errors,function(key,val){
			
			$('<li></li>',{'class' : 'error'}).text(val.type.substr(0,1).toUpperCase() + val.type.substr(1) + " - " + val.message).delay(500).appendTo(properties.selector + ' .status');
			
		});
		
	}
	
	var loaderOn = function() {
		
		$(properties.selector + " .loader").fadeIn();
		
	}
	
	var loaderOff = function() {
		
		$(properties.selector + " .loader").fadeOut();
		
	}
	
	var showSuccess = function(message) {
		
		clearStatus();
		
		loaderOff();
		
		setMessage(message);
		
	}
	
	var clearStatus = function() {
		
		$(properties.selector + " .modal-body .status").html("");
		
	}
	
	var setMessage = function(message) {

		$(properties.selector + " .message").text(message);
		
	}
	
	var hideConfirm = function(){
		
		$(properties.selector + " .confirm").hide();
		
	}
	
	var showConfirm = function(){
		
		$(properties.selector + " .confirm").show();
		
	}
		
	
	
	return {
		openModal : openModal,
		addUpdate : addUpdate,
		loaderOn : loaderOn,
		loaderOff : loaderOff,
		addError : addError,
		showSuccess : showSuccess,
		clearStatus : clearStatus,
		setMessage : setMessage,
		hideConfirm : hideConfirm,
		showConfirm : showConfirm,
		hideButtons : hideButtons,
		showButtons : showButtons,
	}
	
})();