MigrationBuilder = (function(){
	
	var properties = {
		
		altPathModalSelector : '#alt-migration-modal',
		migrationList : [],
		altSource : "",
		altDestination : ""
		
	}
	
	var setAltSource = function(path) {
		
		properties.altSource = path;
		
		return true;
		
	}
	
	var getAltSource = function() {
		
		return properties.altSource;
		
	}
	
	var setAltDestination = function(path) {
		
		properties.altDestination = path;
		
		return true;
		
	}
	
	var getAltDestination = function() {
		
		return properties.altDestination;
		
	}
	
	var addMigrationEntry = function(sourcePath,destinationPath) {
		
		if (typeof destinationPath == 'undefined') {

			destinationPath = sourcePath;

		}
		
		var list = properties.migrationList;
		var source,destination;
		var endEarly = false;
		
		for (var i = 0; i < list.length; list++) {
			
			source = list[i]['source'];
			destination = list[i]['destination'];
			
			console.log("Source " + source);
			console.log("Destination " + destination);
			
			if (sourcePath === source) {
				
				console.log("replacing the source destination");
				
				list[i]['destination'] = destinationPath;
				
				endEarly = true;
				
				break;
				
			}
			
		}
		
		if (endEarly) return true;
		
		var migrationEntry = {"source" : sourcePath, 'destination' : destinationPath};
		
		properties.migrationList.push(migrationEntry);
		
	}
	
	var removeMigrationEntry = function(sourcePath) {
		
		getMigrationList().forEach(function(val,key) {
			
			if (val.source == sourcePath) {
				
				getMigrationList().splice(key,1)
				
			}
			
		});
		
	}
	
	var getMigrationList = function() {
		
		return properties.migrationList;
		
	}
	
	var selectChildren = function($element,selected) {
		
		var $currentElement,
			selector;
		
		$element.children('.file').each(function(key,val) {
			
			$currentElement = $(val).find('input');
			
			if (selected) {
				
				$currentElement.prop('checked',selected);
				
				addMigrationEntry($currentElement.val());
				
			}
			
			if (!selected) {
				
				$currentElement.prop('checked',selected);
				
				removeMigrationEntry($currentElement.val());
				
			}
	
			if ($currentElement.attr('data-target')) {
				
				selector = $currentElement.attr('data-target');
				
				selectChildren($(selector),selected);
				
			}
			
		});
		
	}
	
	var openAltPathModal = function(sourcePath) {
		
		$(properties.altPathModalSelector).modal('show');
		
		$(properties.altPathModalSelector + ' .builder input').prop('checked',false);
		
	}
	
	var closeAltPathModal = function() {
		
		$(properties.altPathModalSelector).modal('hide');
		
	}
	
	return {
		addMigrationEntry : addMigrationEntry,
		removeMigrationEntry : removeMigrationEntry,
		getMigrationList : getMigrationList,
		selectChildren : selectChildren,
		openAltPathModal : openAltPathModal,
		closeAltPathModal : closeAltPathModal,
		setAltSource : setAltSource,
		getAltSource : getAltSource,
		setAltDestination : setAltDestination,
		getAltDestination : getAltDestination
	}
	
})();