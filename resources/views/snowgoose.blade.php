<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta charset="utf-8">
  
	<title>SnowGoose - Migration Assistant</title>
	
	<link rel="shortcut icon" href="/images/favicon.jpg" type="image/x-icon">
	
	<meta name="description" content="" />
  	<meta name="keywords" content="" />
	<meta name="robots" content="" />
	
	<link rel="stylesheet" href="{{ elixir("css/app.css") }}" >
	
</head>
<body>
	<div class="container">
	
		@include('_partials.project_head')
		
		<div id="app-panel" class="row">

			<div class="col-md-4 col-md-offset-4 text-center user-input">
				
				@include('_partials.project_selector')
				
				@include('_partials.update_version_selector')
				
				@include('_partials.migration_builder')
				
				@include('_partials.run_migration')

			</div>

		</div>

	</div>
	
	@include('modals.alt_migration_path')
	
	@include('modals.status_update')
	
	<script>
		window.csrfToken = '<?php echo csrf_token(); ?>';
	</script>
	
	<script src="{{ elixir("js/app.js") }}"></script>
	
</body>
</html>