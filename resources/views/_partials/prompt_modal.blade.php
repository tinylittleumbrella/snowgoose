<div id="prompt-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body text-center">
      	<img class="loader" src="/images/default-hourglass.svg" />
      	<p class="message"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        <button id="confirm-update-version" type="button" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
</div>