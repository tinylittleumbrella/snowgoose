<div id="migration-builder" class="form-group user-input init-hide">

	<h5><b>Which files would you like to move over?</b></h5>

	<h6><i>Specify a new migration path by clicking the pencil icon.</i></h6>

	<ul class="builder"></ul>

</div>