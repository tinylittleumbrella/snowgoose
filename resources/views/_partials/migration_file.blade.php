
@foreach($tree as $path => $node)
	
	@if(is_array($node))

		<li class="file">
		
			<input class="select-children"{{ ($alt_path) ? 'name="alt-path-radio"' : false }}  data-target="#{{ str_replace(['.','/','-'], '_', $path) }}" type="{{ (!$alt_path) ? "checkbox" : "radio" }}" value="{{ $path }}">
		
			<span class="glyphicon glyphicon-folder-close"></span> 
			
			<span  class="directory-trigger" data-target="#{{ str_replace(['.','/','-'], '_', $path) }}">{{ basename($path) }}/</span>
			
			<span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>

			@if(!$alt_path)
			
				<button type="button" class="btn btn-default btn-xs pull-right launch-alt-path" data-source="{{ $path }}">
				
					<span class="custom-migration-path glyphicon glyphicon-pencil" aria-hidden="true"></span>
					
				</button>
				
			@endif
		
		</li>
		
		<ul id="{{ str_replace(['.','/','-'], '_', $path) }}" class="directory">
		
			@include('_partials.migration_file',['tree' => $node['children'], 'alt_path' => $alt_path])
		
		</ul>
		
	@else

		<li class="file">
			
			<input class="{{ ($node == "*") ? "select-all-files" : ""}}" type="{{ (!$alt_path) ? "checkbox" : "radio" }}" value="{{ $node }}"><span class="glyphicon {{ ($node == "*") ? "glyphicon glyphicon-th" : "glyphicon-file"}}"></span> {{ basename($node) }}
			 
			 @if(!$alt_path)

				 <button type="button" class="btn btn-default btn-xs pull-right launch-alt-path" data-source="{{ $node }}">
				 
				 	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
				 	
				 </button>
			 
			 @endif
			 
		</li>

	
	@endif
	
@endforeach
