<div id="project-path" class="form-group">
				
	<label for="project-target-path">Enter the path to your Laravel project.</label>
	
	<input id="project-target-path" type="text" placeholder="/path/to/project" />
	
	<button id="project-submit-path" class="btn btn-primary">Load</button>

	<p class="status"></p>

</div>

<div id="get-started" class="form-group init-hide user-input">

	<button class="btn btn-success">Let's Get Started.</button>

</div>

<ul id="project-properties" class="init-hide user-input">

	<li class="header"><b>Current Project Details</b></li>

	<li class="name"><b>Name: </b><span class="value"></span></li>
	
	<li class="description"><b>Description: </b><span class="value"></span></li>
	
	<li class="version"><b>Version: </b><span class="value"></span></li>
	
	<li class="path"><b>Path: </b><span class="value"></span></li>
	
</ul>