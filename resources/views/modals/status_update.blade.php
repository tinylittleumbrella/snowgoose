<div id="status-update" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="title"></h4>
      </div>
      <div class="modal-body text-center">
      	<img class="loader" src="/images/default-hourglass.svg" />
      	<p class="message"></p>
      	<ul class="status"></ul>
      </div>
      <div class="modal-footer">
<!--         <button type="button" class="btn btn-primary confirm" data-dismiss="modal">Ok</button> -->
        <button type="button" class="btn btn-default deny" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>