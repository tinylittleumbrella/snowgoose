<div id="alt-migration-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Where do you want this to land?</h4>
      </div>
      <div class="modal-body">
     	
     	<ul class="builder"></ul>
     	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary save-alt-path">Save</button>
      </div>
    </div>
  </div>
</div>